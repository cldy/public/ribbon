using Autofac;
using Ribbon.Core;
using Ribbon.Data;
using Ribbon.Modules.Projects;

namespace Ribbon.ModuleLoader
{
    public class ModuleLoaderModule : Module
    {
        protected override void Load (ContainerBuilder builder)
        {
            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new DataModule());
            builder.RegisterModule(new ProjectsModule());
        }
    }
}