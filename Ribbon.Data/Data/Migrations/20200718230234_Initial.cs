﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ribbon.Data.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up (MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Project",
                table => new
                {
                    Id = table.Column<byte[]>("bytea", nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Project", x => x.Id); });
        }

        protected override void Down (MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "Project");
        }
    }
}