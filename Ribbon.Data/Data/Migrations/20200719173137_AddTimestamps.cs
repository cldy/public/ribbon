﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ribbon.Data.Data.Migrations
{
    public partial class AddTimestamps : Migration
    {
        protected override void Up (MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                "CreatedAt",
                "Project",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                "UpdatedAt",
                "Project",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down (MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                "CreatedAt",
                "Project");

            migrationBuilder.DropColumn(
                "UpdatedAt",
                "Project");
        }
    }
}