using Autofac;
using Ribbon.Core.Data;

namespace Ribbon.Data
{
    public class DataModule : Module
    {
        protected override void Load (ContainerBuilder builder)
        {
            base.Load(builder);


            builder.RegisterContext<AppDbContext>("App");
        }
    }
}