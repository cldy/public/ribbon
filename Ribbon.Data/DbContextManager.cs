using System;
using System.Collections.Generic;
using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;

namespace Ribbon.Data
{
    public static class DbContextManager
    {
        public static void RegisterContext<TContext> (this ContainerBuilder builder, string connectionStringName) where TContext : DbContext
        {
            builder.Register(context =>
            {
                var serviceProvider = context.Resolve<IServiceProvider>();
                var configuration = context.Resolve<IConfiguration>();
                var dbContextOptions = new DbContextOptions<TContext>(new Dictionary<Type, IDbContextOptionsExtension>());
                var optionsBuilder = new DbContextOptionsBuilder<TContext>(dbContextOptions)
                    .UseApplicationServiceProvider(serviceProvider)
                    .UseNpgsql(configuration.GetConnectionString(connectionStringName), options => { options.MigrationsAssembly("Ribbon.Data"); });

                return optionsBuilder.Options;
            }).As<DbContextOptions<TContext>>().InstancePerLifetimeScope();

            builder.Register(context => context.Resolve<DbContextOptions<TContext>>())
                .As<DbContextOptions>()
                .InstancePerLifetimeScope();

            builder.RegisterType<TContext>()
                .AsSelf()
                .InstancePerLifetimeScope();
        }
    }
}