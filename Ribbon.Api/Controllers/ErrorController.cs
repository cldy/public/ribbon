using Microsoft.AspNetCore.Mvc;

namespace Ribbon.Api.Controllers
{
    [ApiController]
    public class ErrorController : ControllerBase
    {
        [Route("/error")]
        public IActionResult Error ()
        {
            return Problem();
        }
    }
}