using Autofac;
using Ribbon.ModuleLoader;

namespace Ribbon.Api
{
    public class ApiModule : Module
    {
        protected override void Load (ContainerBuilder builder)
        {
            builder.RegisterModule(new ModuleLoaderModule());
        }
    }
}