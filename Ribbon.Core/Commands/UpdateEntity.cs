using System;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.CQRS;
using Ribbon.Core.Base.Entities;

namespace Ribbon.Core.Commands
{
    public class UpdateEntity<TDto, TEntity, TContext> : ICommand<Ulid>
        where TDto : BaseDto<TEntity>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        public UpdateEntity (Ulid id, TDto dto)
        {
            Id = id;
            Dto = dto;
        }

        public Ulid Id { get; }
        public TDto Dto { get; }
    }

    // public class UpdateEntity<TDto, TEntity> : UpdateEntity<TDto, TEntity, AppDbContext>
    //     where TDto : BaseDto<TEntity>
    //     where TEntity : BaseEntity
    // {
    //     public UpdateEntity (Ulid id, TDto dto) : base(id, dto)
    //     { }
    // }
}