using System;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.CQRS;
using Ribbon.Core.Base.Entities;

namespace Ribbon.Core.Commands
{
    public class DeleteEntity<TEntity, TContext> : ICommand<Ulid>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        public DeleteEntity (Ulid id)
        {
            Id = id;
        }

        public Ulid Id { get; }
    }

    // public class DeleteEntity<TEntity> : DeleteEntity<TEntity, AppDbContext>
    //     where TEntity : BaseEntity
    // {
    //     public DeleteEntity (Ulid id) : base(id)
    //     { }
    // }
}