using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.Entities;
using Ribbon.Core.Data;

namespace Ribbon.Core.Commands.Handlers
{
    public class UpdateEntityHandler<TDto, TEntity, TContext> : IRequestHandler<UpdateEntity<TDto, TEntity, TContext>, Ulid>
        where TDto : BaseDto<TEntity>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        private readonly IRepository<TEntity, TContext> _repository;

        public UpdateEntityHandler (IRepository<TEntity, TContext> repository)
        {
            _repository = repository;
        }

        #region IRequestHandler<UpdateEntity<TDto,TEntity,TContext>,Ulid> Members

        public async Task<Ulid> Handle (UpdateEntity<TDto, TEntity, TContext> request, CancellationToken cancellationToken)
        {
            var entity = await _repository.Update(request.Id, request.Dto);
            await _repository.SaveChanges(cancellationToken);

            return entity.Id;
        }

        #endregion
    }
}