using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.Entities;
using Ribbon.Core.Data;

namespace Ribbon.Core.Commands.Handlers
{
    public class CreateEntityHandler<TDto, TEntity, TContext> : IRequestHandler<CreateEntity<TDto, TEntity, TContext>, Ulid>
        where TDto : BaseDto<TEntity>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        private readonly IRepository<TEntity, TContext> _repository;

        public CreateEntityHandler (IRepository<TEntity, TContext> repository)
        {
            _repository = repository;
        }

        #region IRequestHandler<CreateEntity<TDto,TEntity,TContext>,Ulid> Members

        public async Task<Ulid> Handle (CreateEntity<TDto, TEntity, TContext> request, CancellationToken cancellationToken)
        {
            var entity = await _repository.Create(request.Dto, cancellationToken);
            await _repository.SaveChanges(cancellationToken);

            return entity.Id;
        }

        #endregion
    }
}