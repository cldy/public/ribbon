using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.Entities;
using Ribbon.Core.Data;

namespace Ribbon.Core.Commands.Handlers
{
    public class DeleteEntityHandler<TEntity, TContext> : IRequestHandler<DeleteEntity<TEntity, TContext>, Ulid>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        private readonly IRepository<TEntity, TContext> _repository;

        public DeleteEntityHandler (IRepository<TEntity, TContext> repository)
        {
            _repository = repository;
        }

        #region IRequestHandler<DeleteEntity<TEntity,TContext>,Ulid> Members

        public async Task<Ulid> Handle (DeleteEntity<TEntity, TContext> request, CancellationToken cancellationToken)
        {
            var entity = await _repository.Delete(request.Id);
            await _repository.SaveChanges(cancellationToken);

            return entity.Id;
        }

        #endregion
    }
}