using System;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.CQRS;
using Ribbon.Core.Base.Entities;

namespace Ribbon.Core.Commands
{
    public class CreateEntity<TDto, TEntity, TContext> : ICommand<Ulid>
        where TDto : BaseDto<TEntity>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        public CreateEntity (TDto dto)
        {
            Dto = dto;
        }

        public TDto Dto { get; }
    }

    // public class CreateEntity<TDto, TEntity> : CreateEntity<TDto, TEntity, AppDbContext>
    //     where TDto : BaseDto<TEntity>
    //     where TEntity : BaseEntity
    // {
    //     public CreateEntity (TDto dto) : base(dto)
    //     { }
    // }
}