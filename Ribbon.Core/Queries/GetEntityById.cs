using System;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.CQRS;
using Ribbon.Core.Base.Entities;

namespace Ribbon.Core.Queries
{
    // ReSharper disable once UnusedTypeParameter
    public class GetEntityById<TEntity, TContext> : IQuery<TEntity>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        public GetEntityById (Ulid id)
        {
            Id = id;
        }

        public Ulid Id { get; }
    }

    // public class GetEntityById<TEntity> : GetEntityById<TEntity, AppDbContext>
    //     where TEntity : BaseEntity
    // {
    //     public GetEntityById (Ulid id) : base(id)
    //     {}
    // }
}