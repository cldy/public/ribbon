using System.Linq;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.CQRS;
using Ribbon.Core.Base.Entities;

namespace Ribbon.Core.Queries
{
    // ReSharper disable once UnusedTypeParameter
    public class GetEntities<TEntity, TContext> : IQuery<IQueryable<TEntity>>
        where TEntity : BaseEntity
        where TContext : DbContext
    { }

    // public class GetEntities<TEntity> : GetEntities<TEntity, AppDbContext>
    //     where TEntity : BaseEntity
    // {}
}