using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.Entities;
using Ribbon.Core.Data;

namespace Ribbon.Core.Queries.Handlers
{
    public class GetEntityByIdHandler<TEntity, TContext> : IRequestHandler<GetEntityById<TEntity, TContext>, TEntity>
        where TContext : DbContext
        where TEntity : BaseEntity
    {
        private readonly IRepository<TEntity, TContext> _repository;

        public GetEntityByIdHandler (IRepository<TEntity, TContext> repository)
        {
            _repository = repository;
        }

        #region IRequestHandler<GetEntityById<TEntity,TContext>,TEntity> Members

        public async Task<TEntity> Handle (GetEntityById<TEntity, TContext> request, CancellationToken cancellationToken)
        {
            return await _repository.Find(request.Id);
        }

        #endregion
    }
}