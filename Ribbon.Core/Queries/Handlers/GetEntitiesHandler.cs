using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.Entities;
using Ribbon.Core.Data;

namespace Ribbon.Core.Queries.Handlers
{
    public class GetEntitiesHandler<TEntity, TContext> : IRequestHandler<GetEntities<TEntity, TContext>, IQueryable<TEntity>>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        private readonly IRepository<TEntity, TContext> _repository;

        public GetEntitiesHandler (IRepository<TEntity, TContext> repository)
        {
            _repository = repository;
        }

        #region IRequestHandler<GetEntities<TEntity,TContext>,IQueryable<TEntity>> Members

        public Task<IQueryable<TEntity>> Handle (GetEntities<TEntity, TContext> request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_repository.Queryable());
        }

        #endregion
    }
}