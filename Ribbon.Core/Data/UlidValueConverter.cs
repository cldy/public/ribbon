using System;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Ribbon.Core.Data
{
    public class UlidToBytesConverter : ValueConverter<Ulid, byte[]>
    {
        private static readonly ConverterMappingHints DefaultHints = new ConverterMappingHints(16);

        public UlidToBytesConverter (ConverterMappingHints? mappingHints = null)
            : base(
                x => x.ToByteArray(),
                x => new Ulid(x),
                DefaultHints.With(mappingHints))
        { }
    }

    public class UlidToStringConverter : ValueConverter<Ulid, string>
    {
        private static readonly ConverterMappingHints DefaultHints = new ConverterMappingHints(26);

        public UlidToStringConverter (ConverterMappingHints? mappingHints = null)
            : base(
                x => x.ToString(),
                x => Ulid.Parse(x),
                DefaultHints.With(mappingHints))
        { }
    }
}