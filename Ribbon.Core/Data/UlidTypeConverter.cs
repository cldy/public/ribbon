using System;
using System.ComponentModel;
using System.Globalization;

namespace Ribbon.Core.Data
{
    public class UlidTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom (ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override bool CanConvertTo (ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(Ulid);
        }

        public override object ConvertTo (ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (!CanConvertTo(destinationType) || !CanConvertFrom(value.GetType())) throw new NotSupportedException();

            return Ulid.Parse((string) value);
        }

        public override object ConvertFrom (ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (!CanConvertFrom(value.GetType())) throw new NotSupportedException();

            return Ulid.Parse((string) value);
        }
    }
}