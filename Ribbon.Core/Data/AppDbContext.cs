using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.Entities;

namespace Ribbon.Core.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext (DbContextOptions<AppDbContext> options) : base(options)
        { }

        protected override void OnModelCreating (ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var entities =
                from assembly in AppDomain.CurrentDomain.GetAssemblies().AsParallel()
                from type in assembly.GetTypes().AsParallel()
                where type.IsDefined(typeof(EntityAttribute), false)
                select type;

            foreach (var entity in entities)
            {
                modelBuilder
                    .Entity(entity)
                    .Property<Ulid>(nameof(BaseEntity.Id))
                    .HasColumnType("bytea")
                    .HasConversion(new UlidToBytesConverter())
                    .HasValueGenerator<UlidValueGenerator>();
                modelBuilder
                    .Entity(entity)
                    .Property<DateTime>(nameof(BaseEntity.CreatedAt));
                modelBuilder
                    .Entity(entity)
                    .Property<DateTime>(nameof(BaseEntity.UpdatedAt));
            }
        }
    }
}