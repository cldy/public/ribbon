using System;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;

namespace Ribbon.Core.Data
{
    public class UlidValueGenerator : ValueGenerator<Ulid>
    {
        public override bool GeneratesTemporaryValues { get; } = false;

        public override Ulid Next (EntityEntry entry)
        {
            return Ulid.NewUlid();
        }
    }
}