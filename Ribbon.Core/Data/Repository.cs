using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.Entities;

namespace Ribbon.Core.Data
{
    public class Repository<TEntity, TContext> : IRepository<TEntity, TContext>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        private readonly TContext _context;
        private readonly IMapper _mapper;

        public Repository (TContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        #region IRepository<TEntity,TContext> Members

        public async ValueTask<TEntity> Create<TDto> (TDto dto, CancellationToken cancellationToken) where TDto : BaseDto<TEntity>
        {
            var entity = _mapper.Map<TDto, TEntity>(dto);
            var now = DateTime.UtcNow;
            entity.CreatedAt = now;
            entity.UpdatedAt = now;

            return (await _context.Set<TEntity>().AddAsync(entity, cancellationToken)).Entity;
        }

        public async ValueTask<TEntity> Replace<TDto> (Ulid id, TDto dto, CancellationToken cancellationToken) where TDto : BaseDto<TEntity>
        {
            if (!await _context.Set<TEntity>().AnyAsync(e => e.Id == id, cancellationToken)) throw new Exception("Entity not found");

            var entity = _mapper.Map<TDto, TEntity>(dto);

            entity.UpdatedAt = DateTime.UtcNow;

            return _context.Set<TEntity>().Update(entity).Entity;
        }

        public async ValueTask<TEntity> Update<TDto> (Ulid id, TDto dto) where TDto : BaseDto<TEntity>
        {
            var entity = await Find(id);

            if (entity == null) throw new Exception("Entity not found");

            entity = _mapper.Map(dto, entity);
            entity.UpdatedAt = DateTime.UtcNow;

            return _context.Set<TEntity>().Update(entity).Entity;
        }

        public async ValueTask<TEntity> Delete (Ulid id)
        {
            var entity = await _context.Set<TEntity>().FindAsync(id);

            return _context.Remove(entity).Entity;
        }

        public IQueryable<TEntity> Queryable ()
        {
            return _context.Set<TEntity>().AsQueryable().AsNoTracking();
        }

        public async ValueTask<TEntity> Find (Ulid id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public async Task<int> SaveChanges (CancellationToken cancellationToken)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        #endregion
    }

    public class Repository<TEntity> : Repository<TEntity, AppDbContext>
        where TEntity : BaseEntity
    {
        public Repository (AppDbContext context, IMapper mapper) : base(context, mapper)
        { }
    }

    public interface IRepository<TEntity, TContext>
        where TEntity : BaseEntity
        where TContext : DbContext
    {
        public ValueTask<TEntity> Create<TDto> (TDto dto, CancellationToken cancellationToken) where TDto : BaseDto<TEntity>;

        public ValueTask<TEntity> Create<TDto> (TDto dto) where TDto : BaseDto<TEntity>
        {
            return Create(dto, CancellationToken.None);
        }

        public ValueTask<TEntity> Replace<TDto> (Ulid id, TDto dto, CancellationToken cancellationToken) where TDto : BaseDto<TEntity>;

        public ValueTask<TEntity> Replace<TDto> (Ulid id, TDto dto) where TDto : BaseDto<TEntity>
        {
            return Replace(id, dto, CancellationToken.None);
        }

        public ValueTask<TEntity> Update<TDto> (Ulid id, TDto dto) where TDto : BaseDto<TEntity>;
        public ValueTask<TEntity> Delete (Ulid id);
        public IQueryable<TEntity> Queryable ();
        public ValueTask<TEntity> Find (Ulid id);
        public Task<int> SaveChanges (CancellationToken cancellationToken);

        public Task<int> SaveChanges ()
        {
            return SaveChanges(CancellationToken.None);
        }
    }
}