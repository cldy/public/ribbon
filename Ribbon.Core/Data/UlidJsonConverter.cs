using System;
using Newtonsoft.Json;

namespace Ribbon.Core.Data
{
    public class UlidJsonConverter : JsonConverter<Ulid>
    {
        public override Ulid ReadJson (JsonReader reader, Type objectType, Ulid existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            return reader.Value == null ? Ulid.Empty : Ulid.Parse((string) reader.Value);
        }

        public override void WriteJson (JsonWriter writer, Ulid value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToString());
        }
    }
}