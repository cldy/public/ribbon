using System;

namespace Ribbon.Core.Data
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EntityAttribute : Attribute
    { }
}