﻿using System;
using Autofac;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using MediatR;
using Ribbon.Core.Commands.Handlers;
using Ribbon.Core.Data;
using Ribbon.Core.Queries.Handlers;

namespace Ribbon.Core
{
    public class CoreModule : Module
    {
        protected override void Load (ContainerBuilder builder)
        {
            builder.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            builder
                .RegisterType<Mediator>()
                .As<IMediator>()
                .InstancePerLifetimeScope();

            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            // Register repository
            builder
                .RegisterGeneric(typeof(Repository<,>))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            // Register non-generic request handlers
            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(IRequestHandler<>))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            // Register generic query handlers
            builder
                .RegisterGeneric(typeof(GetEntityByIdHandler<,>))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder
                .RegisterGeneric(typeof(GetEntitiesHandler<,>))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            // Register generic command handlers
            builder
                .RegisterGeneric(typeof(CreateEntityHandler<,,>))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder
                .RegisterGeneric(typeof(UpdateEntityHandler<,,>))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder
                .RegisterGeneric(typeof(DeleteEntityHandler<,>))
                .AsImplementedInterfaces()
                .InstancePerDependency();
        }
    }
}