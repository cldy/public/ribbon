using MediatR;

namespace Ribbon.Core.Base.CQRS
{
    public interface ICommand<out TResult> : IRequest<TResult>
    { }

    public abstract class Command<TResult> : ICommand<TResult>
    { }
}