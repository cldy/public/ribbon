using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Ribbon.Core.Base.CQRS
{
    public interface IQueryHandler<in TQuery, TResult> : IRequestHandler<TQuery, TResult> where TQuery : IQuery<TResult>
    { }

    public abstract class QueryHandler<TQuery, TResult> : IQueryHandler<TQuery, TResult> where TQuery : IQuery<TResult>
    {
        #region IQueryHandler<TQuery,TResult> Members

        public virtual Task<TResult> Handle (TQuery query, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}