using MediatR;

namespace Ribbon.Core.Base.CQRS
{
    public interface IQuery<out TResult> : IRequest<TResult>
    { }

    public abstract class Query<TResult> : IQuery<TResult>
    { }
}