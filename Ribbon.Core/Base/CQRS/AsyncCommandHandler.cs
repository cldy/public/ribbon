using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Ribbon.Core.Base.CQRS
{
    public interface IAsyncCommandHandler<in TAsyncCommand> : IRequestHandler<TAsyncCommand> where TAsyncCommand : IAsyncCommand
    { }

    public abstract class AsyncCommandHandler<TAsyncCommand> : IAsyncCommandHandler<TAsyncCommand> where TAsyncCommand : IAsyncCommand
    {
        #region IAsyncCommandHandler<TAsyncCommand> Members

        public virtual Task<Unit> Handle (TAsyncCommand command, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}