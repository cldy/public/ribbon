using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Ribbon.Core.Base.CQRS
{
    public interface ICommandHandler<in TCommand, TResult> : IRequestHandler<TCommand, TResult> where TCommand : ICommand<TResult>
    { }

    public abstract class CommandHandler<TCommand, TResult> : ICommandHandler<TCommand, TResult> where TCommand : ICommand<TResult>
    {
        #region ICommandHandler<TCommand,TResult> Members

        public virtual Task<TResult> Handle (TCommand command, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}