using MediatR;

namespace Ribbon.Core.Base.CQRS
{
    public interface IAsyncCommand : IRequest
    { }

    public abstract class AsyncCommand : IAsyncCommand
    { }
}