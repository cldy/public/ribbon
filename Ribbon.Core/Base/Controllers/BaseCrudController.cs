using System;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ribbon.Core.Base.Entities;
using Ribbon.Core.Commands;
using Ribbon.Core.Data;
using Ribbon.Core.Queries;

namespace Ribbon.Core.Base.Controllers
{
    [ApiController]
    public abstract class BaseCrudController<TContext, TEntity, TCreateDto, TUpdateDto> : ControllerBase
        where TContext : DbContext
        where TEntity : BaseEntity
        where TCreateDto : BaseDto<TEntity>
        where TUpdateDto : BaseDto<TEntity>
    {
        private readonly AbstractValidator<TCreateDto> _createDtoValidator;
        private readonly IMediator _mediator;
        private readonly AbstractValidator<TUpdateDto> _updateDtoValidator;

        protected BaseCrudController (
            IMediator mediator,
            AbstractValidator<TCreateDto> createDtoValidator,
            AbstractValidator<TUpdateDto> updateDtoValidator
        )
        {
            _mediator = mediator;
            _createDtoValidator = createDtoValidator;
            _updateDtoValidator = updateDtoValidator;
        }

        [HttpGet]
        public async Task<ActionResult<PagedResult<TEntity>>> List (
            int limit = 20,
            int page = 1,
            Order order = Order.Asc,
            string orderBy = "id"
        )
        {
            if (limit > 100) throw new ArgumentException("Maximum items per page is 100", nameof(limit));

            if (page < 1) throw new ArgumentException("Minimum page number is 1", nameof(page));

            var projects = await _mediator.Send(new GetEntities<TEntity, TContext>());

            return projects
                .OrderBy($"{orderBy} {order}")
                .PageResult(page, limit);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TEntity>> Get ([FromRoute] Ulid id)
        {
            var project = await _mediator.Send(new GetEntityById<TEntity, TContext>(id));

            if (project == null) return NotFound();

            return project;
        }

        [HttpPost]
        public async Task<ActionResult<Ulid>> Create ([FromBody] TCreateDto dto)
        {
            if (_createDtoValidator != null)
            {
                var validation = await _createDtoValidator.ValidateAsync(dto);

                if (!validation.IsValid) return BadRequest(validation.Errors);
            }

            var id = await _mediator.Send(new CreateEntity<TCreateDto, TEntity, TContext>(dto));

            return CreatedAtAction("Get", new {id}, id);
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult<Ulid>> Update ([FromBody] TUpdateDto dto, [FromRoute] Ulid id)
        {
            if (_updateDtoValidator != null)
            {
                var validation = await _updateDtoValidator.ValidateAsync(dto);

                if (!validation.IsValid) return BadRequest(validation.Errors);
            }

            var updatedId = await _mediator.Send(new UpdateEntity<TUpdateDto, TEntity, TContext>(id, dto));

            return Ok(updatedId);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Ulid>> Delete ([FromRoute] Ulid id)
        {
            var deletedId = await _mediator.Send(new DeleteEntity<TEntity, TContext>(id));

            return Ok(deletedId);
        }
    }

    public abstract class BaseCrudController<TEntity, TCreateDto, TUpdateDto> : BaseCrudController<AppDbContext, TEntity, TCreateDto, TUpdateDto>
        where TEntity : BaseEntity
        where TCreateDto : BaseDto<TEntity>
        where TUpdateDto : BaseDto<TEntity>
    {
        protected BaseCrudController (
            IMediator mediator,
            AbstractValidator<TCreateDto> createDtoValidator,
            AbstractValidator<TUpdateDto> updateDtoValidator
        ) : base(mediator, createDtoValidator, updateDtoValidator)
        { }
    }

    public enum Order
    {
        Asc,
        Desc
    }
}