using System;

namespace Ribbon.Core.Base.Entities
{
    public interface IBaseEntity
    {
        public Ulid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    public abstract class BaseEntity : IBaseEntity
    {
        #region IBaseEntity Members

        public Ulid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        #endregion
    }
}