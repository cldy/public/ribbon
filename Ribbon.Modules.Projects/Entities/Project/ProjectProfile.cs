using AutoMapper;

namespace Ribbon.Modules.Projects.Entities.Project
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile ()
        {
            CreateMap<CreateProjectDto, Project>()
                .ReverseMap();
            CreateMap<UpdateProjectDto, Project>()
                .ForAllMembers(opt =>
                    opt.Condition((src, dest, srcMember, destMember) => srcMember != null));
        }
    }
}