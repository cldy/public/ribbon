using System.ComponentModel.DataAnnotations;
using FluentValidation;
using Ribbon.Core.Base.Entities;

namespace Ribbon.Modules.Projects.Entities.Project
{
    public class CreateProjectDto : BaseDto<Project>
    {
        [Required] public string Name { get; set; } = null!;

        [Required] public string Description { get; set; } = null!;
    }

    public class CreateProjectDtoValidator : AbstractValidator<CreateProjectDto>
    {
        public CreateProjectDtoValidator ()
        {
            RuleFor(dto => dto.Name).NotEmpty().MinimumLength(3);
            RuleFor(dto => dto.Description).NotEmpty().MinimumLength(6);
        }
    }
}