using FluentValidation;
using Ribbon.Core.Base.Entities;

namespace Ribbon.Modules.Projects.Entities.Project
{
    public class UpdateProjectDto : BaseDto<Project>
    {
        public string? Name { get; set; }

        public string? Description { get; set; }
    }

    public class UpdateProjectDtoValidator : AbstractValidator<UpdateProjectDto>
    {
        public UpdateProjectDtoValidator ()
        {
            RuleFor(dto => dto.Name).MinimumLength(3);
            RuleFor(dto => dto.Description).MinimumLength(6);
        }
    }
}