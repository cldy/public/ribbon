using System.ComponentModel.DataAnnotations;
using Ribbon.Core.Base.Entities;
using Ribbon.Core.Data;

namespace Ribbon.Modules.Projects.Entities.Project
{
    [Entity]
    public class Project : BaseEntity
    {
        [Required] public string Name { get; set; } = null!;

        [Required] public string Description { get; set; } = null!;
    }
}