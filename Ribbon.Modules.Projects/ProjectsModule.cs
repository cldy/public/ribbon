using Autofac;
using FluentValidation;
using MediatR;

namespace Ribbon.Modules.Projects
{
    public class ProjectsModule : Module
    {
        protected override void Load (ContainerBuilder builder)
        {
            // Register request handlers
            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(AbstractValidator<>))
                .AsSelf()
                .InstancePerDependency();
        }
    }
}