using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Ribbon.Core.Base.Controllers;
using Ribbon.Modules.Projects.Entities.Project;

namespace Ribbon.Modules.Projects.Controllers
{
    [Route("projects/[Controller]")]
    [ApiController]
    public class ProjectsController : BaseCrudController<Project, CreateProjectDto, UpdateProjectDto>
    {
        public ProjectsController (
            IMediator mediator,
            AbstractValidator<CreateProjectDto> createDtoValidator,
            AbstractValidator<UpdateProjectDto> updateDtoValidator
        ) : base(mediator, createDtoValidator, updateDtoValidator)
        { }

        [HttpGet("hello")]
        public string Hello ()
        {
            return "ProjectsController: Hello!";
        }
    }
}